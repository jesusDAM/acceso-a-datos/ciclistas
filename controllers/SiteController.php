<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Lleva;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta1(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsulta2(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo = "Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'")->orWhere("nomequipo = 'Amore Vita'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
    public function actionConsulta3(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo = "Artiach" or nomequipo = "Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
    public function actionConsulta4a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad not between 25 and 30"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad NOT BETWEEN 25 AND 30",
        ]);
    }
    
    public function actionConsulta4(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from ciclista where edad not between 25 and 30')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad NOT BETWEEN 25 AND 30',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad NOT BETWEEN 25 AND 30",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad between 28 and 32")->andWhere("nomequipo = 'Banesto'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta5(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from ciclista where edad between 28 and 32 and nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = "Banesto"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta6a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->distinct()->where("char_length(nombre) > 8"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
        ]);
    }
    
    public function actionConsulta6(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct nombre) from ciclista where char_length(nombre) > 8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
        ]);
    }
    
    public function actionConsulta7a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("ucase(nombre) as nombre, dorsal")->distinct(),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT UCASE(nombre) AS nombre, dorsal FROM ciclista",
        ]);
    }
    
    public function actionConsulta7(){
        $numero = Yii::$app->db
                ->createCommand('select count(*) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT UCASE(nombre) AS nombre, dorsal FROM ciclista',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT UCASE(nombre) AS nombre, dorsal FROM ciclista",
        ]);
    }
    
    public function actionConsulta8a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => LLeva::find()->select("dorsal")->distinct()->where("código = 'mge'"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
    public function actionConsulta8(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from lleva where código = "mge"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
    public function actionConsulta9a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->distinct()->where("altura > 1500"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta9(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct nompuerto) from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta10a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8")->orWhere("altura between 1800 and 3000"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    public function actionConsulta10(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 or altura between 1800 and 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    public function actionConsulta11a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente > 8")->andWhere("altura between 1800 and 3000"),
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    public function actionConsulta11(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 and altura between 1800 and 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination' => [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
