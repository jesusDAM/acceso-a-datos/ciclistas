<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ejercicios de consultas 1</h1>

        <p class="lead">Acceso a datos</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 1</h2>

                <p>Listar las edades de los ciclistas (sin repetidos)</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta1a">Active record</a>
                    <a class="btn btn-default" href="site/consulta1">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consula 2</h2>

                <p>Listar las edades de los ciclistas de Artiach</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta2a">Active record</a>
                    <a class="btn btn-default" href="site/consulta2">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 3</h2>

                <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta3a">Active record</a>
                    <a class="btn btn-default" href="site/consulta3">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 4</h2>

                <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta4a">Active record</a>
                    <a class="btn btn-default" href="site/consulta4">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consula 5</h2>

                <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta5a">Active record</a>
                    <a class="btn btn-default" href="site/consulta5">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 6</h2>

                <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta6a">Active record</a>
                    <a class="btn btn-default" href="site/consulta6">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 7</h2>

                <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta7a">Active record</a>
                    <a class="btn btn-default" href="site/consulta7">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consula 8</h2>

                <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta8a">Active record</a>
                    <a class="btn btn-default" href="site/consulta8">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consulta 9</h2>

                <p>Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>

                <p>
                    <a class="btn btn-primary" href="site/consulta9a">Active record</a>
                    <a class="btn btn-default" href="site/consulta9">DAO</a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Consulta 10</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta10a">Active record</a>
                    <a class="btn btn-default" href="site/consulta10">DAO</a>
                </p>
            </div>
            <div class="col-lg-4">
                <h2>Consula 11</h2>

                <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>

                <p>
                    <a class="btn btn-primary" href="site/consulta11a">Active record</a>
                    <a class="btn btn-default" href="site/consulta11">DAO</a>
                </p>
            </div>
        </div>
    </div>
</div>
